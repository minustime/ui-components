/*
This JS code drives some of the UI componets. 
Developers using this UI library should consider this script as reference only and use the tools provided by their
front-end framework to add interactivity to these modules and their pages.
*/

$(document).ready(function() {

    function isMobile() {
        return Modernizr.mq('(max-width: 744px)');
    }
    
    var navPrimary = (function() {

        // See: https://davidwalsh.name/javascript-debounce-function
        function debounce(func, wait, immediate) {
            var timeout;
            return function() {
                var context = this, args = arguments;
                var later = function() {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        }

        // We'll assume there's only one primary navigation
        function init() {

            // Open subpanel on desktop
            $(document).off('mouseenter.primaryNavItem').on('mouseenter.primaryNavItem', '.nav-primary__item.js-has-subpanel', function(){
                if(!isMobile()) {
                    $(this).addClass('is-active');
                    $('.page-wrapper').addClass('is-shifted');
                }
            });

            // Close subpanel on desktop
            $(document).off('mouseleave.primaryNavItem').on('mouseleave.primaryNavItem', '.nav-primary__item.js-has-subpanel', function() {
                if(!isMobile()) {
                    $(this).removeClass('is-active');
                    $('.page-wrapper').removeClass('is-shifted');
                }
            });

            // Toggle subpanel on mobile
            $(document).off('click.primaryNavItem').on('click.primaryNavItem', '.nav-primary__item.js-has-subpanel', function(e) {
                if(isMobile()) {
                    if ($(e.target).hasClass('aolcore-icon--back-arrow')) {
                        $(this).removeClass('is-active');
                    }
                    else {
                        $(this).addClass('is-active');
                    }
                }
            });

            // Open subpanel on mobile
            $(document).off('click.primaryNavItemSubpanel').on('click.primaryNavItemSubpanel', '.js-primary-nav-toggle', function() {
                if(isMobile()) {
                    var primaryNav = $('.nav-primary');
                    var pageWrapper = $('.page-wrapper');
                    var items = $('.nav_primary__item.js-has-subpanel');
                    var isPrimaryNavEnabled = primaryNav.hasClass('is-active');
                    primaryNav.toggleClass('is-active', !isPrimaryNavEnabled);
                    pageWrapper.toggleClass('is-shifted', !isPrimaryNavEnabled);
                    items.removeClass('is-active');
                    $(this).toggleClass('is-active', !isPrimaryNavEnabled);
                }
            });

            // Close subpanel when clicking back button
            $(document).off('click.primaryNavItemSubpanelClose').on('click.primaryNavItemSubpanelClose', '.js-nav-primary-subpanel-close', function() {
                $(this).parents('.nav-primary__subpanel').removeClass('is-active');
            });

            // Close navigation, panels when resizing window
            var onResize = debounce(function() {
                if(!isMobile()) {
                    $('.page-wrapper').removeClass('is-shifted');
                    $('.nav-primary').removeClass('is-active');
                    $('.nav-primary__subpanel').removeClass('is-active');
                }
            }, 25);

            if(window.attachEvent) {
                window.attachEvent('onresize', onResize);
            }
            else if(window.addEventListener) {
                window.addEventListener('resize', onResize);
            }
        }

        init()

    }());

    // Dropdown on/off
    $(document).off('click.toggleAccountMenu touchstart.toggleAccountMenu').on('click.toggleAccountMenu touchstart.toggleAccountMenu', '.js-profile-options-toggle', function(e) {
        // e.preventDefault();
        e.stopPropagation();
        
        var $this = $(this);
        var $profileOptions = $this.find('> .profile__options');
        var isProfileOptionsEnabled = $profileOptions.hasClass('is-active');

        $profileOptions.toggleClass('is-active');

        if (isProfileOptionsEnabled) {
            $(document).off('click.closeAccountMenu');
        } else {
            $(document).one('click.closeAccountMenu', ':not(.profile, .profile__name, .profile__options, .profile__option, .profile__link, .profile__icon, .js-profile-options-toggle, .profile)', function (evt) {
                evt.stopPropagation();
                $this.trigger('click.toggleAccountMenu');
            });
        }
    });

    // Remove the prompt
    $(document).off('click.prompt').on('click.prompt', '.js-prompt-action', function() {
        $(this).parents('.prompt:first').fadeOut();
    });

    // Reveal
    $(document).off('click.reveal').on('click.reveal', '.reveal .js-reveal-toggle', function(e) {
        var parent = $(this).parents('.reveal');
        parent.toggleClass('is-active', !parent.hasClass('is-active'));
    });

    // Footer
    $(document).off('click.footerNav').on('click.footerNav', '.nav-footer .js-nav-footer-toggle', function(e) {
        if(isMobile()) {
            var $this = $(this);
            var $wrapper = $this.parents('.nav-footer__sections');
            var $thisSection = $this.parents('.nav-footer__section');
            var $siblingsActive = $wrapper.find('.nav-footer__section').not($thisSection).filter('.is-active');

            $thisSection.add($siblingsActive).toggleClass('is-active');
        }
    });

    // Tooltips
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'hover click'
        }); 
    });

});