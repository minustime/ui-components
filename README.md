# AOL Core UI Components

## Requirements

* NodeJs - latest stable version

## Setup

1. Install dependencies: `$ npm install`
2. Build the project: `$ npm run build`

## Usage

To build the project files, run the following commnands:

```
// Compile .pug templates into HTML
$ npm run build:html

// Compile .scss files into CSS
$ npm run build:css

// Compile .pug templates, .scss files, copy static assets
$ npm run build

```

To compile the project files automatically as you work, run the following commands:

```
// Compile .pug templates into HTML automatically
$ npm run watch:html

// Compile .scss files into CSS automatically
$ npm run watch:css

// Compile .pug templates and .scss files
$ npm run watch
```

## Project files

```
├── README.md
├── dist                                            # project's files compiled from source
│   ├── assets                                      # static assets
│   │   ├── css
│   │   │   ├── main.css
│   │   │   └── main.css.map
│   │   ├── fonts
│   │   └── js
│   │       ├── main.js
│   │       └── vendor
│   ├── components                                  # compiled HTML components
│   └── samples                                     # sample files
│       ├── sample-components.html
│       ├── sample-content-containers.html
│       ├── sample-html-elements.html
│       └── sample-icons.html
├── package.json
└── src                                             # project source files
    ├── assets                                      # static assets
    │   ├── fonts
    │   │   ├── AolLogo.eot
    │   │   ├── AolLogo.svg
    │   │   ├── AolLogo.ttf
    │   │   └── AolLogo.woff
    │   ├── js
    │   │   ├── main.js
    │   │   └── vendor                              # 3rd party scripts
    │   │       ├── html5shiv.min.js
    │   │       ├── jquery-1.8.3.min.js
    │   │       ├── modernizr-custom.js
    │   │       └── respond.min.js
    │   └── scss
    │       ├── base                                # global base styles used throughout project
    │       │   ├── _accessibility.scss             # accessibility related classes
    │       │   ├── _fonts.scss                     # font family definitions
    │       │   ├── _icons.scss                     # icon definitions
    │       │   ├── _resets.scss                    # global resets 
    │       │   └── _typography.scss                # styles for headers, paragraphs, etc.
    │       ├── components                          # styles for individual components
    │       │   ├── _buttons.scss
    │       │   ├── _card.scss
    │       │   ├── _checkbox-group.scss
    │       │   ├── _contained-overflow.scss
    │       │   ├── _forms.scss
    │       │   ├── _header.scss
    │       │   ├── _hero.scss
    │       │   ├── _lists.scss
    │       │   ├── _menu.scss
    │       │   ├── _nav-footer.scss
    │       │   ├── _nav-primary.scss
    │       │   ├── _nav-secondary.scss
    │       │   ├── _notification.scss
    │       │   ├── _profile.scss
    │       │   ├── _prompt.scss
    │       │   ├── _radio-group.scss
    │       │   └── _reveal.scss
    │       ├── helpers                             # sass helpers used throughout project
    │       │   ├── _functions.scss                 # custom functions
    │       │   ├── _mixins.scss                    # custom mixins
    │       │   └── _variables.scss                 # variables used throughout the project
    │       ├── layout                              # styles that define page level containers
    │       │   └── _content.scss
    │       ├── main.scss                           # main scss file, pulls all scss files together
    │       └── vendors                             # 3rd party styles
    │           └── _normalize.scss
    ├── components                                  # pug templates for all components
    │   ├── card.pug
    │   ├── checkbox-group.pug
    │   ├── checkbox-group__two-col--desktop-up.pug
    │   ├── checkbox-group__two-col.pug
    │   ├── contained-overflow.pug
    │   ├── header.pug
    │   ├── hero.pug
    │   ├── menu.pug
    │   ├── nav-footer.pug
    │   ├── nav-primary.pug
    │   ├── notification.pug
    │   ├── profile.pug
    │   ├── prompt.pug
    │   ├── radio-group.pug
    │   ├── radio-group__two-col--desktop-up.pug
    │   ├── radio-group__two-col.pug
    │   └── reveal.pug
    └── samples                                     # implementation examples
        ├── includes
        │   ├── head.pug
        │   ├── mixins.pug
        │   └── nav-primary.pug
        ├── my-account-cancel.pug
        ├── my-account-product.pug
        ├── my-account-subscriptions.pug
        ├── sample-components.pug
        ├── sample-content-containers.pug
        ├── sample-html-elements.pug
        └── sample-icons.pug
```

## Reference

* http://getbem.com
* http://sass-lang.com
* https://sass-guidelin.es/#the-7-1-pattern
* https://pugjs.org

## TO-DO

* Move module values (eg. Hex colors) to Sass variables
* Look into using SVG instead of font icons _with_ IE8 support

